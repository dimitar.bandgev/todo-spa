import React from 'react'

const TodoTable = () => {

    const todos = [
        {id: 1, name: 'Task 1', description: "Task 1 description", status: "Done", createdBy: "User 1"},
        {id: 2, name: 'Task 2', description: "Task 2 description", status: "Done", createdBy: "User 2"},
        {id: 3, name: 'Task 3', description: "Task 3 description", status: "Todo", createdBy: "User 3"},
        {id: 4, name: 'Task 4', description: "Task 4 description", status: "Done", createdBy: "User 4"},

        // Add more todos as needed
    ];

    const handleEdit = (id) => {
        // Handle edit functionality for the todo with the given id
        console.log(`Edit todo with ID ${id}`);
    };

    const handleDelete = (id) => {
        // Handle delete functionality for the todo with the given id
        console.log(`Delete todo with ID ${id}`);
    };

    return (<div className="container">
        <table className="table">
            <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Name</th>
                <th scope="col">Description</th>
                <th scope="col">Status</th>
                <th scope="col">Created By</th>
                <th scope="col">Actions</th>
            </tr>
            </thead>
            <tbody>
            {todos.map((todo) => (<tr key={todo.id}>
                <td>{todo.id}</td>
                <td>{todo.name}</td>
                <td>{todo.description}</td>
                <td>{todo.status}</td>
                <td>{todo.createdBy}</td>
                <td>
                    <div className="btn-group">
                        <button className="btn btn-primary" onClick={() => handleEdit(todo.id)}>
                            Edit
                        </button>
                        <button className="btn btn-danger" onClick={() => handleDelete(todo.id)}>
                            Delete
                        </button>
                    </div>
                </td>
            </tr>))}
            </tbody>
        </table>
    </div>);
};

export default TodoTable;