import React from 'react'
import TodoTable from "./TodoTable";

const handleCreate = () => {
    // Handle create functionality
    console.log('Create Todo');
};

const TodoPage = () => {
    return (<div className="container">
        <h1> Todo Page </h1>
        <div style={{textAlign: 'left'}}>
            <button className="btn btn-success mb-3" onClick={handleCreate}>
                Create
            </button>
        </div>
        <TodoTable/>
    </div>)
};

export default TodoPage;
