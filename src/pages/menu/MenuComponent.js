import React, {useState} from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import TodoPage from "../todo/TodoPage";
import UserPage from "../user/UserPage";


const MenuComponent = () => {
    const [activeItem, setActiveItem] = useState('User');

    const handleItemClick = (item) => {
        setActiveItem(item);
    };

    return (
        <div>
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <div className="collapse navbar-collapse" id="navbarNav">
                    <ul className="navbar-nav">
                        <li className={`nav-item ${activeItem === 'User' ? 'active' : ''}`}
                            onClick={() => handleItemClick('User')}>
                            <a className="nav-link" href="#user">User</a>
                        </li>
                        <li className={`nav-item ${activeItem === 'Todo' ? 'active' : ''}`}
                            onClick={() => handleItemClick('Todo')}>
                            <a className="nav-link" href="#todo"> Todo</a>
                        </li>
                    </ul>
                </div>
            </nav>
            {activeItem === 'User' && <UserPage/>}
            {activeItem === 'Todo' && <TodoPage/>}
        </div>
    );
};
export default MenuComponent;