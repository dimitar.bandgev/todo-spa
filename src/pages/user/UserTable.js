import React from 'react'

const UserTable = () => {

    const users = [{id: 1, username: 'john_doe'}, {id: 2, username: 'jane_smith'}, {
        id: 3,
        username: 'james_brown'
    }, // Add more users as needed
    ];

    const handleEdit = (id) => {
        // Handle edit functionality for the user with the given id
        console.log(`Edit user with ID ${id}`);
    };

    const handleDelete = (id) => {
        // Handle delete functionality for the user with the given id
        console.log(`Delete user with ID ${id}`);
    };

    return (<div className="container">
        <table className="table">
            <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Username</th>
                <th scope="col">Actions</th>
            </tr>
            </thead>
            <tbody>
            {users.map((user) => (<tr key={user.id}>
                <td>{user.id}</td>
                <td>{user.username}</td>
                <td>
                    <div className="btn-group">
                        <button className="btn btn-primary" onClick={() => handleEdit(user.id)}>
                            Edit
                        </button>
                        <button className="btn btn-danger" onClick={() => handleDelete(user.id)}>
                            Delete
                        </button>
                    </div>
                </td>
            </tr>))}
            </tbody>
        </table>
    </div>);
};

export default UserTable;