import React, {useState} from 'react';
import 'bootstrap/dist/css/bootstrap.css';

const UserModal = () => {
    const [showModal, setShowModal] = useState(false);
    const [username, setUsername] = useState('');

    const handleInputChange = (e) => {
        setUsername(e.target.value);
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        // Perform submit logic here (e.g., API call to create a user)
        console.log(username);
        // Reset form and close modal
        setUsername('');
        setShowModal(false);
    };

    return (
        <div className="container">
            <button className="btn btn-success mb-3" onClick={() => setShowModal(true)}>
                Create
            </button>

            {/* Modal */}
            {showModal && (
                <div className="modal" tabIndex="-1" role="dialog" style={{display: 'block'}}>
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title">Create User</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close"
                                        onClick={() => setShowModal(false)}>
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <form onSubmit={handleSubmit}>
                                    <div className="form-group">
                                        <label htmlFor="username">Username</label>
                                        <input type="text" className="form-control" id="username" name="username"
                                               value={username} onChange={handleInputChange} required/>
                                    </div>
                                    <button type="submit" className="btn btn-success mt-2">
                                        Create
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            )}
        </div>
    );
};

export default UserModal;