import React from 'react'
import UserTable from "./UserTable";
import UserModal from "./UserModal";

const handleCreate = () => {
    // Handle create functionality
    console.log('Create user');
};

const UserPage = () => {
    return (
        <div className="container">
            <h1> User Page </h1>
            <div style={{textAlign: 'left'}}>
                <UserModal/>
            </div>
            <UserTable/>
        </div>
    )
};

export default UserPage;
