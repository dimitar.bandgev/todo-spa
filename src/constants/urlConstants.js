export const REACT_APP_HOST_BACKEND =  window.location.origin;

// API TODO
export const GET_ALL_TODOS = '/api/v1/todo';
export const GET_ONE_TODO = (id) => `/api/v1/todo/${id}`;
export const CREATE_TODO = '/api/v1/todo';
export const UPDATE_ONE_TODO = (id) => `/api/v1/todo/${id}`;
export const DELETE_ONE_TODO = (id) => `/api/v1/todo/${id}`;

// API USER
export const GET_ALL_USERS = '/api/v1/user';
export const GET_ONE_USER = (id) => `/api/v1/user/${id}`;
export const CREATE_USER = '/api/v1/user';
export const UPDATE_ONE_USER = (id) => `/api/v1/user/${id}`;
export const DELETE_ONE_USER = (id) => `/api/v1/user/${id}`;

// Application routes paths
export const ROUTES_PATHS = {
    HOME: "/",
    TODO: "/todo",
    USER: "/user"
}