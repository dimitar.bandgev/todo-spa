import {ROUTES_PATHS} from "../constants/urlConstants";
import TodoPage from "../pages/todo/TodoPage";
import UserPage from "../pages/user/UserPage";
import HomePage from "../pages/HomePage/HomePage";


const todo = [
    {"path": ROUTES_PATHS.HOME, "page": <HomePage/>},
    {"path": ROUTES_PATHS.TODO, "page": <TodoPage/>},

];

const user = [
    {"path": ROUTES_PATHS.HOME, "page": <HomePage/>},
    {"path": ROUTES_PATHS.USER, "page": <UserPage/>},
]

export {
    todo,
    user
};
