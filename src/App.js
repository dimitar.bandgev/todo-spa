import './App.css';


import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import MenuComponent from "./pages/menu/MenuComponent";

function App() {
    return (
        <div className="App">
            <MenuComponent/>
        </div>
    );
}

export default App;
